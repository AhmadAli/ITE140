import pandas as pd

df = pd.read_csv("world_population.csv")

df.index.name = "IC"
df.columns.name = "Props"
df.sort_values(["Continent"])

print(df.loc[200, :])

tf = (df["Rank"] <= 10) & (df["Continent"] == "Asia")
print(df.loc[tf, :])
