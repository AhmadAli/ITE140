# Where our data comes from

[kaggle.com](https://www.kaggle.com/datasets/iamsouravbanerjee/world-population-dataset?resource=download)

We found this data set on a website called kaggle.com were it was aggergated for us and easily usable in our coding exercise but a deeper look into it takes you to another website where they got the data.

[world pop review](https://worldpopulationreview.com/)

even thought this website maybe the link provided to us from kaggle its still not the orginal source of information as they do send us down another hyperlink to the 

[US world census](https://www.census.gov/popclock/world)

and one might think a two link trail is long enough but no there is another link leading us to the 

[international Database](https://www.census.gov/data-tools/demo/idb/#/country?COUNTRY_YEAR=2022&COUNTRY_YR_ANIM=2022)

This data base is nice because it has a bunch of graphs about the world population detailing stuff like the gender ratio per age group in a population, the annual growth rate. and thats the end of my cyber chase.

## Some extra websites

on my to the orginal source of our dataset I went past a few addtional sources and here are some of them.

[UN News](https://news.un.org/en/story/2022/07/1122272)

[U.S News](https://www.usnews.com/opinion/blogs/robert-schlesinger/2014/12/31/us-population-2015-320-million-and-world-population-72-billion)

[History.com](https://www.history.com/news/how-fast-is-the-worlds-population-growing)

[UN.org](https://www.un.org/development/desa/pd/sites/www.un.org.development.desa.pd/files/wpp2022_summary_of_results.pdf)

[Journal of International Women's studies](https://vc.bridgew.edu/jiws/vol21/iss6/27/)

