import pandas as pd

data = [["Mark", 55, "Italy", 4.5, "Europe"],
        ["John", 33, "USA", 6.7, "America"],
        ["Tim", 41, "USA", 3.9, "America"],
        ["Jenny", 12, "Germany", 9.0, "Europe"]]
df = pd.DataFrame(data=data,
        columns=["name", "age", "country", "score", "continent"],
        index=[1001, 1000, 1002, 1003])
print(df)
print(df.info())
# By calling the info method, you will get some basic information,
# most importantly the number of data points and the data types for each column

print(df.index)

print(df.index.name = "user_id")

print(df.reset_index())
# "reset_index" turns the index into a column, replacing the index with defualt inde# x. this corresponds to the data from the beginnig that we loaded from excel. 
print(df.reset_index().set_index("name"))
# reset_index turns the column "user_id" into a regular column a "set_index" turns the column "name" into the index
print(df.reindex([999, 1000, 1001, 1004]))
# reindex will take over all rows that match the new index and will introduce rows with missing values (NaN) where no information exists. Index elements that you leave away will be dropped.
print(df.sort_index())
# to sort an index, use the sort_index method

print(df.sort_values(["continent", "age"]))
# to sort the rows by one or more columns, use sort_values

print(df.columns)

print(df.columns.name = "properties")

print(df.rename(columns=("name": "First Name", "age" : "Age")))
# renaming columns 

print(df.drop(columns=["name", "country"],
    index=[1000, 1003]))

print(df.T) # shortcut for df.transpose()

print(df.loc[[1001, 1002], "age"])

print(df.loc[:1002, ["name", "country"]])

print(df.loc[:, column_selection])

print(df[column_selection])

import numpy as np

print(matplotlib inline)

data = (pd.DataFrame(data=np.random.rand(4, 4) * 100000,
                             index=["Q1", "Q2", "Q3", "Q4"],
                             columns=["East", "West", "North", "South"])
         data.index.name = "Quarters"
         data.columns.name = "Region"
         data)
print(data.plot())

pd.options.plotting.backend = "potly"

print(data.plot())

print data.plot.bar(barmode="group")


