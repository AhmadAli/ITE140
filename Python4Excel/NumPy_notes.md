# Ahmads Notes

## What is NumPy

Numpy is the core package for scientific communications in python, it provides support for array based calculations and linear algebra. 

### basic vocab 

* Array Based Calculations: Array formula is a formula that can perfom multiple calculations at once on one or more items.

* Linear Algebra: a branch of mathmatics concerned with structurs like closed operations.

## NumPy array 

to perfom array based calculations with nested lists a loop is necceray 

example: In [1]: matrix = [[1, 2, 3],
                           [4, 5, 6],
                           [7, 8, 9,]]
In [2]: [[i +1 for i in row] for row in matrix]

out [2]: [[2, 3, 4], [5, 6, 7], [8, 9, 10,]]

Although this method is not recomended for large sets of data instead use NumPy.
NumPy can make calculations of large data a hundred times faster than a normal python loop this is because NumPy uses code that was written in C or fortan whichis much faster than python

### Some extra info 

* Indexes are both 1 dimensional and 2 dimensional arrays

## Some more examples 

In [3]: First       
