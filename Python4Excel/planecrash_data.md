import pandas as pd

# Plane crash data

Ill be looking over a dataset with a bunch of information on differnt incidents of planes crashes since the year 1948. This datasetincludes informstion like date, time, location, operator, flight number, route, type of aircraft, registration number, cn/in number of persons on board, fatalities, ground fatalities, and a summary of each accident. over 5,000 plane crashes were recorded in this dataset.

## Deadliest crashes

a quick scan through our dataset shows us that most of the accidents had no ground fatalties and very few onboard deaths but here is the DEADLIEST one I could find.

* Date: 04/15/2002

* Location: Busan, South Korea

* Route: Bejing - Busan

* Type of aircraft: Boeing B-767-200er

* Registration of craft: B-2552

* Onboard: 166

* Ground deaths: 0

* Total deaths: 128

## Earliest Crash

This is the earliest crash that I could find

* Date: 09/17/1908

* Location: Fort Myer, Virginia

* Route: Demonstration

* Type of aircraft: Wright Flyer III

* Registration of craft: NULL

* Onboard: 2

* Ground deaths: 0

* Total deaths: 1:
