# Defintions 

* Jupyter notebooks-is Web-based interactive computational environment for creating notebook documents. Jupyter Notebooks is built using serveral libraries, including IPython, ZeroMq,   
* pandas-a software library written for the Python programming language for data manipulation and analysis
* interface-a device or program that allows a user to communicate with a computer without any complex coding knowledge
* data sets-a collection of related pieces of data. 
* statisticst-he practice or science of collecting and analyzing numerical data in large quantities, especially for the purpose of inferring proportions in a whole from those in a representative sample 
* time series- a collection of obeservations collected through repeated measurments of time  
* vectorization-optimizing an algorithm so that it can use SIMD instructions in the proccessors 
* data alignment-Data alignment is the process of harmonisation of the underlying concepts and definitions of Variables or units in order to produce values for measurement of Variables, which can be related to the maximum extent possible
* cleaning data-The processof polishing a data set by getting rid of errors dupilcations unwanted info etc).
* aggregation- getting a group of things together
* descriptive statistics- information that sums up an entire data set 
* visualization-the act of seeing an image in your head. 

## In Depth definitions 

In depth statistics 

First what is a statistic. statistics is the science of collecting organizing, analysing, and interpeting data in order to make decisions.

### Background Info

What is data

data consists of information coming from observations, counts, measurments, or responses.

what is a population

a population is the entire group that you want to draw conclusions about.

what is a sample

A sample is a subset, or part of a population.

### Different types of sampling

* Simple random sampling- a sampling method were everyone in a pobulation has an equal random chance of being chosen.

* Stratified random sampling- A population is divided into groups with similar features and then a random sample of porportional is selected from each group to create a sample.

* Systematic Random Sampling- sample memebersare selected according to a random starting point and a fixed, periodic interval.

* Cluster Random Sample- Divides the population into mini populations groupd. (clusters, and select all of the memebers of the cluster and instead of sampling indiciduals from a group you just sample an entire group. 
